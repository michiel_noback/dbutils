package nl.bioinf.noback.db_utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class DbCredentials {
    public static String getMySQLdbPassword() throws PasswordRetrievalException, FileNotFoundException, IOException {

        DbUser u;
        try {
            u = getMySQLuser();
        } catch (NoSuchFieldException e) {
            throw new PasswordRetrievalException(e.getMessage());
        }
        return u.databasePassword;

    }

    /**
     * Use the default config file to create a DbUser
     */
    public static DbUser getMySQLuser() throws FileNotFoundException, IOException, NoSuchFieldException {
        return  getMySQLuser(System.getProperty("user.home") + File.separator + getMySQLuser(".my.cnf"));
    }

    /**
     *
     * @return @throws FileNotFoundException
     * @throws IOException
     * @throws NoSuchFieldException
     */
    public static DbUser getMySQLuser(String configFile) throws FileNotFoundException, IOException, NoSuchFieldException {
        String passPropsFileName = configFile;
        Properties properties = new Properties();
        properties.load(new FileInputStream(passPropsFileName));

        DbUser u = new DbUser();
        if (!properties.containsKey("user")) {
            throw new NoSuchFieldException("field \"user\" is not found in the configuration");
        }
        u.userName = properties.getProperty("user");

        if (!properties.containsKey("password")) {
            throw new NoSuchFieldException("field \"password\" is not found in the configuration");
        }
        u.databasePassword = properties.getProperty("password");

        if (!properties.containsKey("host")) {
            throw new NoSuchFieldException("field \"host\" is not found in the configuration");
        }
        u.host = properties.getProperty("host");

        // Port is often not separately configured, asume default of 3306
        u.port = "3306";
        if (properties.containsKey("port")) {
            u.port = properties.getProperty("port");
        }

        if (!properties.containsKey("database")) {
            throw new NoSuchFieldException("field \"database\" is not found in the configuration");
        }
        u.databaseName = properties.getProperty("database");

        return u;
    }

}
